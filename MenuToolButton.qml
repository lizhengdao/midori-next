import QtQuick 2.10
import QtQuick.Controls 2.13

ToolButton {
    property Menu menu
    property alias acceptedButtons: mouse.acceptedButtons

    // Sensible defaults
    icon.width: 16
    icon.height: 16
    background.implicitWidth: padding

    MouseArea {
        id: mouse
        anchors.fill: parent
        acceptedButtons: menu ? Qt.RightButton : Qt.NoButton
        onClicked: {
            menu.y = parent.height
            menu.modal = true
            menu.open()
        }
    }
}
